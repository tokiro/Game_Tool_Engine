#!/usr/bin/env python3

import sys
import pygame
import copy
from pygame.locals import *
from Arbitre import *

def click(event, pos, pos_bout):
    return event.type == MOUSEBUTTONDOWN and event.button == 1 and event.pos[0] >= pos[0] and event.pos[0] < pos_bout[0] and event.pos[1] >= pos[1] and event.pos[1] < pos_bout[1]

class Map:

    def __init__(self, filename):
        self.load(filename)

    def load(self, filename):
        self.filename = filename
        with open(filename, 'r') as infile:
            data = infile.read()
        map_ = data.splitlines()
        self.map_ = [list(map_[i]) for i in range(0, 19)]

    def save(self):
        with open(self.filename, 'w') as infile:
            infile.write('\n'.join([''.join(self.map_[i]) for i in range(0, 19)]))

    def show(self):
        print('\n'.join([''.join(self.map_[i]) for i in range(0, 19)]))

    def clean(self):
        with open(self.filename, 'w') as infile:
            infile.write('+++++++++++++++++++\n' * 19)

    def countWhite(self):
        c = 0
        for y in self.map_:
            for x in y:
                if x == 'O':
                    c += 1
        return c

    def countBlack(self):
        c = 0
        for y in self.map_:
            for x in y:
                if x == 'X':
                    c += 1
        return c


class Scene:

    def __init__(self):
        self.scene = None
        self.volume_value = 0.5
        self.playing_music = True

class Button:

    def __init__(self, pos, pos_bout, sprite, font, text, action):
        self.pos = pos
        self.pos_bout = pos_bout
        self.sprite = sprite
        self.font = font
        self.text = text
        self.action = action

    def click(self, win, event, scene, map_path):
        exec(self.action)

    def draw(self, win, scene):
        if self.font and self.text:
            win.blit(self.text, (self.pos[0], self.pos[1]))
        elif self.sprite:
            win.blit(self.sprite, (self.pos[0], self.pos[1]))


class Case:

    def __init__(self, pos, pos_bout):
        self.pos = pos
        self.pos_bout = pos_bout
        self.activate = True

    def click(self, win, event, scene, map_):
        if self.activate == False:
            return None
        x = int((self.pos[0] - 456) / 53)
        y = int((self.pos[1] - 36) / 53)
        if map_.map_[y][x] == '+' and not scene.scene.tour:
            old = copy.deepcopy(map_.map_)
            map_.map_[y][x] = 'X'
            ret = scene.scene.arbitre.validPlayerMove(scene.scene.oldPlay, (y, x), old, map_.map_)
            if ret == 0:
                map_.map_[y][x] = 'X'
                scene.scene.oldPlay = [y,x]
                scene.scene.tour = True
                scene.scene.placed_black =  scene.scene.placed_black + 1
                scene.scene.background = pygame.image.load("./Textures/pvp_2.png").convert()
                scene.scene.error_activated = False
            else:
                map_.map_[y][x] = '+'
                scene.scene.error = ret
                scene.scene.error_activated = True
        elif map_.map_[y][x] == '+' and scene.scene.tour:
            old = copy.deepcopy(map_.map_)
            map_.map_[y][x] = 'O'
            ret = scene.scene.arbitre.validPlayerMove(scene.scene.oldPlay, (y, x), old, map_.map_)
            if ret == 0:
                map_.map_[y][x] = 'O'
                scene.scene.oldPlay = [y,x]
                scene.scene.tour = False
                scene.scene.placed_white =  scene.scene.placed_white + 1
                scene.scene.background = pygame.image.load("./Textures/pvp_1.png").convert()
                scene.scene.error_activated = False
            else:
                map_.map_[y][x] = '+'
                scene.scene.error = ret
                scene.scene.error_activated = True
        map_.save()


class SimpleSprite:

    def __init__(self, pos, sprite):
        self.pos = pos
        self.sprite = sprite

    def draw(self, win):
        win.blit(self.sprite, (self.pos[0], self.pos[1]))

class Menu:

    def __init__(self, map_path, scene):
        self.obj = []
        pygame.mouse.set_cursor(*pygame.cursors.tri_left)
        ##pygame.mixer.music.load("Assets/menu.mp3")
        ##pygame.mixer.music.play(-1)
        ##if scene.playing_music == False:
        ##    pygame.mixer.music.set_volume(0)
        ##else:
        ##    pygame.mixer.music.set_volume(scene.volume_value)
        self.map_path = map_path
        self.background = pygame.image.load("./Textures/main_menu_error.png").convert()
        self.obj.append(Button([491,295], [1425, 421],None, None, None, "scene.scene = VersusPlayer(map_path, scene)"))
        self.obj.append(Button([491,487], [1425, 612], None, None, None, "print('Clic ! J1 vs IA')"))
        self.obj.append(Button([491,680], [1425, 804], None, None, None, "scene.scene = Options(map_path)"))
        self.obj.append(Button([491,874], [1425, 997], None, None, None, "scene.scene = None"))

    def draw(self, win, scene):
        win.blit(self.background, (0,0))
        # for elem in self.obj:
        #     elem.draw(win, scene)

    def update(self, event, win, scene):
        if event.type == KEYDOWN and event.key == K_ESCAPE:
            scene.scene = None
            return None
        for elem in self.obj:
            if click(event, elem.pos, elem.pos_bout):
                elem.click(win, event, scene, self.map_path)


class VersusIa:

    def __init__(self):
        self.background = pygame.image.load("./Textures/fond.jpg").convert()
        self.grid = pygame.image.load("./Assets/grid.png").convert()
        # fred
        disc1 = pygame.image.load("./Assets/disc1.png").convert_alpha();
        disc2 = pygame.image.load("./Assets/disc2.png").convert_alpha();
        # fin fred
        self._map = []
        y = 36
        while y < 36 + 1007:
            x = 456
            while x < 456 + 1007:
                #fred
                self._map.append(Case([x,y], [x + 53, y + 53], disc2, disc1))
                # fin fred
#                self._map.append(Case([x,y], [x + 53, y + 53], pygame.image.load("./Assets/disc2.png").convert_alpha(), pygame.image.load("./Assets/disc1.png").convert_alpha()))
                x = x + 53
            y = y + 53
        self.obj = []
        self.font = pygame.font.Font("./Fonts/Sketchtica.ttf", 80)
        self.obj.append(Button([100,900], [404, 967], None, self.font, self.font.render("abandon", True, (200, 200, 200)), "print('Clic ! abandon'); scene.scene = Menu()"))

    def draw(self, win, scene):
        win.blit(self.background, (0, 0))
        win.blit(self.grid, (456, 36))
        # for elem in self._map:
        #     elem.draw(win, scene)
        for elem in self.obj:
            elem.draw(win, scene)

    def update(self, event, win, scene):
        # for elem in self._map:
        #     if click(event, elem.pos, elem.pos_bout):
        #         elem.click(win, event, scene)
        for elem in self.obj:
            if click(event, elem.pos, elem.pos_bout):
                elem.click(win, event, scene)


class VersusPlayer: #norme

    def __init__(self, map_path, scene):
        ##pygame.mixer.music.stop()
        ##pygame.mixer.music.load("Assets/ingame.mp3")
        ##pygame.mixer.music.play(-1)
        ##if scene.playing_music == False: #go init
        ##    pygame.mixer.music.set_volume(0)
        ##else: #init
        ##    pygame.mixer.music.set_volume(scene.volume_value)
        self.background = pygame.image.load("./Textures/pvp_1.png").convert()
        self.white = pygame.image.load("./Assets/disc2.png").convert_alpha()
        self.black = pygame.image.load("./Assets/disc1.png").convert_alpha()
        self.helpBlack = pygame.image.load("./Assets/disc1.1.png").convert_alpha()
        self.helpWhite = pygame.image.load("./Assets/disc2.1.png").convert_alpha()
        self.map_path = map_path
        self.map_file = Map(map_path)
        self.map_file.clean()
        self.map_file.load(map_path)
        self.arbitre = Arbitre(self.map_file)
        self.error_activated = False
        self.error = 0
        self.error_tab = [None, pygame.image.load("./Textures/arbitre_2.png").convert_alpha(), pygame.image.load("./Textures/arbitre_1.png").convert_alpha(), pygame.image.load("./Textures/arbitre_3.png").convert_alpha()]
        self.tour = False
        self.oldTour = True
        self.placed_white = 0
        self.placed_black = 0
        self.eaten_white = 0
        self.eaten_black = 0
        self.case_tab = []
        self.eaten_black_tab = []
        self.eaten_white_tab = []
        self.oldPlay = []
        self.pawn = None
        self.obj = []
        self.obj.append(Button([71,982], [411, 1045], None, None, None, "scene.scene = Menu(map_path, scene)"))
        for posy in range(389, 884, 99):
            self.eaten_white_tab.append(SimpleSprite((140, posy), self.white))
            self.eaten_white_tab.append(SimpleSprite((274, posy), self.white))
        for posy in range(389, 884, 99):
            self.eaten_black_tab.append(SimpleSprite((1590, posy), self.black))
            self.eaten_black_tab.append(SimpleSprite((1726, posy), self.black))
        for y in range(36, 1043, 53):
            for x in range(456, 1463, 53):
                self.case_tab.append(Case([x,y], [x + 53, y + 53]))

    def draw(self, win, scene):
        win.blit(self.background, (0, 0))
        for y in range(0, 19):
            for x in range (0, 19):
                if self.map_file.map_[y][x] == 'O':
                    win.blit(self.white, (self.case_tab[(y * 19) + x].pos[0], self.case_tab[(y * 19) + x].pos[1]))
                elif self.map_file.map_[y][x] == 'X':
                    win.blit(self.black, (self.case_tab[(y * 19) + x].pos[0], self.case_tab[(y * 19) + x].pos[1]))
        for elem in self.obj:
            elem.draw(win, scene)
        for (elem, iterator) in zip(self.eaten_white_tab, range(0, self.eaten_white)):
            elem.draw(win)
        for (elem, iterator) in zip(self.eaten_black_tab, range(0, self.eaten_black)):
            elem.draw(win)
        if self.error_activated:
            win.blit(self.error_tab[self.error], (0, 0))
        if self.pawn:
            win.blit((self.helpWhite if self.tour else self.helpBlack), (456 + (self.pawn[1]*53), 36+(self.pawn[0]*53)))

    def doTurn(self, event, win, scene):
        if self.tour == self.oldTour:
            return None
        sym = 'X' if self.tour else 'O'
        if self.oldPlay:
            self.pawn = self.arbitre.helpPlayer(self.map_file.map_, self.oldPlay[0], self.oldPlay[1])
        if self.tour:
            self.obj[0].pos =  [1920-411, 982]
            self.obj[0].pos_bout =[1920-71, 1045]
        else:
            self.obj[0].pos = [71, 982]
            self.obj[0].pos_bout = [411, 1045]
        self.eaten_white = self.placed_white - self.map_file.countWhite()
        self.eaten_black = self.placed_black - self.map_file.countBlack()
        c = '+'
        if scene.scene.oldPlay:
            c = scene.scene.arbitre.gameEnd(scene.scene.map_file.map_, scene.scene.oldPlay[0], scene.scene.oldPlay[1])
        if c != '+' or scene.scene.eaten_black >= 10 or scene.scene.eaten_white >= 10 or (not self.pawn and self.oldPlay):
            if c == 'O' or scene.scene.eaten_black == 10 or (not self.pawn and not self.tour):
                scene.scene.background = pygame.image.load("./Textures/finish2.png").convert()
            else:
                scene.scene.background = pygame.image.load("./Textures/finish1.png").convert()
            scene.scene.obj[:] = []
            for elem in scene.scene.case_tab:
                elem.activate = False
            scene.scene.obj.append(Button([71, 982], [411, 1045], None, None, None, "scene.scene = VersusPlayer(map_path, scene)"))
            scene.scene.obj.append(Button([1920 - 411, 982], [1920 - 71, 1045], None, None, None, "scene.scene = Menu(map_path, scene)"))
            scene.scene.error_activated = False
        self.map_file.load(self.map_path)
        self.oldTour = self.tour

    def update(self, event, win, scene):
        if event.type == KEYDOWN and event.key == K_ESCAPE:
            scene.scene = None
            return None
        self.doTurn(event, win, scene)
        for elem in self.case_tab:
            if click(event, elem.pos, elem.pos_bout):
                elem.click(win, event, scene, self.map_file)
        for elem in self.obj:
            if click(event, elem.pos, elem.pos_bout):
                elem.click(win, event, scene, self.map_path)

class Options:

    def __init__(self, map_path):
        self.obj = []
        self.map_path = map_path
        self.background_on = pygame.image.load("./Textures/options_menu_gomoku_0.png").convert()
        self.background_off = pygame.image.load("./Textures/options_menu_gomoku_1.png").convert()
        self.Font = pygame.font.Font("./Fonts/Flynn.ttf", 60)
        self.background = self.background_on
        self.obj.append(Button([867,901], [1059, 955], None, None, None, "scene.scene = Menu(map_path, scene)"))
        self.obj.append(Button([1068,709], [1243, 772], None, None, None, "scene.playing_music = False"))
        self.obj.append(Button([1005,576], [1080, 621], None, None, None, "scene.volume_value -= 0.1 if scene.volume_value > 0.0 else 0.0"))
        self.obj.append(Button([1194,574], [1255, 628], None, None, None, "scene.volume_value = scene.volume_value + 0.1 if scene.volume_value < 1.0 else 1.0"))

    def update(self, event, win, scene):
        ##if scene.playing_music == False:
        ##    pygame.mixer.music.set_volume(0)
        ##else:
        ##    pygame.mixer.music.set_volume(scene.volume_value)
        if scene.playing_music == False:
            self.obj[1].action = "scene.playing_music = True"
        elif scene.playing_music == True:
            self.obj[1].action = "scene.playing_music = False"
        if event.type == KEYDOWN and event.key == K_ESCAPE:
            scene.scene = Menu(self.map_path)
            return None
        for elem in self.obj:
            if click(event, elem.pos, elem.pos_bout):
                elem.click(win, event, scene, self.map_path)
        if scene.playing_music == True:
            self.background = self.background_on
        elif scene.playing_music == False:
            self.background = self.background_off

    def draw(self, win, scene):
        win.blit(self.background, (0, 0))
        self.text = self.Font.render(str(int(scene.volume_value * 10)), True, (2, 250, 250))
        win.blit(self.text, (1104, 559))

def main(map_path, screenmode):
    pygame.init()
    scene = Scene()
    win = pygame.display.set_mode((1920, 1080), screenmode)
    scene.scene = Menu(map_path, scene)
    while scene.scene:
        scene.scene.draw(win, scene)
        for event in pygame.event.get():
            if event.type == QUIT or not scene.scene:
                return None
            scene.scene.update(event, win, scene)
        pygame.display.flip()
    return None

if __name__ == '__main__':
    av = sys.argv
    screenmode = FULLSCREEN
    if (len(av) < 2):
        print('Error : Wrong or missing argument\n\t./gomoku [map] {-w}')
        sys.exit()
    if (len(av) == 3 and av[2] == '-w'):
        screenmode = NOFRAME
    main(av[1], screenmode)
    pygame.quit()
