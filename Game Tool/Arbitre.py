#!/usr/bin/env python

import sys
import random
from Gomoku import *

class Arbitre: #j'aimerai bien des for ...

    def __init__(self, map_):
        map__ = map_

    def isLeftUpFree(self, map_, y, x): #help player verif 1/8
        if x > 0 and y > 0 and map_[y-1][x-1] == '+':
            return y-1, x-1
        return None

    def isUpFree(self, map_, y, x): # help player verif 2/8
        if y > 0 and map_[y-1][x] == '+':
            return y-1, x
        return None

    def isRightUpFree(self, map_, y, x): #help player verif 3/8
        if x < 18 and y > 0 and map_[y-1][x+1] == '+':
            return y-1, x+1
        return None

    def isLeftFree(self, map_, y, x): #help player verif 4/8
        if x > 0 and map_[y][x-1] == '+':
            return y, x-1
        return None

    def isRightFree(self, map_, y, x): #help player verif 5/8
        if x < 18 and map_[y][x+1] == '+':
            return y, x+1
        return None

    def isLeftDownFree(self, map_, y, x): #help player verif 6/8
        if x > 0 and y < 18 and map_[y+1][x-1] == '+':
            return y+1, x-1
        return None

    def isDownFree(self, map_, y, x): #help player verif 7/8
        if y < 18 and map_[y+1][x] == '+':
            return y+1, x
        return None

    def isRightDownFree(self, map_, y, x): #help player verif 8/8
        if y < 18 and x < 18 and map_[y+1][x+1] == '+':
            return y+1, x+1
        return None

    def helpPlayer(self, map_, y, x):
        tab = ['self.isLeftUpFree(map_, y, x)', 'self.isUpFree(map_, y, x)', 'self.isRightUpFree(map_, y, x)',\
               'self.isLeftFree(map_, y, x)', 'self.isRightFree(map_, y, x)',\
               'self.isLeftDownFree(map_, y, x)', 'self.isDownFree(map_, y, x)', 'self.isRightDownFree(map_, y, x)']
        postab = []
        for rand in range(0,8):
            res = eval(tab[rand])
            postab.append(res) if res else None
        if not postab:
            return None
        for pawn in postab:
            vMap = copy.deepcopy(map_)
            vMap[pawn[0]][pawn[1]] = 'O' if map_[y][x] == 'X' else 'X'
            if self.gameEnd(vMap, pawn[0], pawn[1]) != '+':
                return pawn
        for pawn in postab:
            vMap = copy.deepcopy(map_)
            vMap[pawn[0]][pawn[1]] = 'O' if map_[y][x] == 'X' else 'X'
            if self.CaptureFunct(pawn[0], pawn[1], vMap) != 0:
                return pawn
        return postab[random.randint(0, len(postab)-1)]

    def checkHori(self, map_, y, x):
        Pierre = map_[y][x]
        save_x = x
        compt = 1
        x -= 1
        while(compt < 5 and x <= 18 and map_[y][x] and map_[y][x] == Pierre): # pardon ? x = -1 -> x -= 1 = map[y][-1..-2..] nan ? ca devrait segf... j'aime pas ca du tout
            compt += 1
            x -= 1
        x = save_x + 1
        while(compt < 5 and x <= 18 and map_[y][x] and map_[y][x] == Pierre):
            compt += 1
            x += 1
        return compt >= 5

    def checkVert(self, map_, y, x):
        Pierre = map_[y][x]
        save_y = y
        compt = 1
        y -= 1
        while (compt < 5 and y <= 18 and map_[y][x] and map_[y][x] == Pierre):
            y -= 1
            compt += 1
        y = save_y + 1
        while (compt < 5 and y <= 18 and map_[y][x] and map_[y][x] == Pierre):
            compt += 1
            y += 1
        return compt >= 5

    def checkDiag2(self, map_, y, x):
        Pierre = map_[y][x]
        save_y = y
        save_x = x
        compt = 1
        y -= 1
        x += 1
        while (compt < 5 and x <= 18 and map_[y][x] and map_[y][x] == Pierre):
            y -= 1
            x += 1
            compt += 1
        y = save_y + 1
        x = save_x - 1
        while (compt < 5 and y <= 18 and map_[y][x] and map_[y][x] == Pierre):
            y += 1
            x -= 1
            compt +=1
        return compt >= 5

    def checkDiag(self, map_, y, x):
        Pierre = map_[y][x]
        save_y = y
        save_x = x
        compt = 1
        y -= 1
        x -= 1
        while (compt < 5 and map_[y][x] and map_[y][x] == Pierre):
            y -= 1
            x -= 1
            compt += 1
        y = save_y + 1
        x = save_x + 1
        while (compt < 5 and y <= 18 and x <= 18 and map_[y][x] and map_[y][x] == Pierre):
            y += 1
            x += 1
            compt +=1
        return compt >= 5

    def checkCaptureVert(self, map_, y, x, flag):
        Pierre = map_[y][x]
        if ((y >= 2 and y <= 17) and
            (map_[y - 1][x] == Pierre) and
            (map_[y - 2][x] != Pierre and map_[y - 2][x] != '+') and
            (map_[y + 1][x] == '+')):
            return True
        if ((y >= 1 and y <= 16) and
            (map_[y + 1][x] == Pierre) and
            (map_[y + 2][x] != Pierre and map_[y + 2][x] != '+') and
            (map_[y - 1][x] == '+')):
            return True
        if ((y >= 1 and y <= 16 and x >= 4 and x <= 18 and flag == 0) and
            (map_[y + 1][x - 1] == Pierre) and
            (map_[y + 2][x - 1] != Pierre and map_[y + 2][x - 1] != '+') and
            (map_[y - 1][x - 1] == '+')):
            return True
        if ((y >= 2 and y <= 17 and x >= 4 and x <= 18 and flag == 0) and
            (map_[y - 1][x - 1] == Pierre) and
            (map_[y - 2][x - 1] != Pierre and map_[y - 2][x - 1] != '+') and
            (map_[y + 1][x - 1] == '+')):
            return True
        if ((y >= 1 and y <= 16 and x >= 0 and x <= 14 and flag == 0) and
            (map_[y + 1][x + 1] == Pierre) and
            (map_[y + 2][x + 1] != Pierre and map_[y + 2][x + 1] != '+') and
            (map_[y - 1][x + 1] == '+')):
            return True
        if ((y >= 2 and y <= 17 and x >= 0 and x <= 14 and flag == 0) and
            (map_[y - 1][x + 1] == Pierre) and
            (map_[y - 2][x + 1] != Pierre and map_[y - 2][x + 1] != '+') and
            (map_[y + 1][x + 1] == '+')):
            return True
        return False

    def checkCaptureDiagH1(self, map_, y, x):
        Pierre = map_[y][x]
        if ((y >= 2 and y <= 17 and x >= 4 and x <= 17) and
            (map_[y - 1][x - 1] == Pierre) and
            (map_[y - 2][x - 2] != Pierre and map_[y - 2][x - 2] != '+') and
            (map_[y + 1][x + 1] == '+')):
            return True
        if ((y >= 1 and y <= 16 and x >= 4 and x <= 17) and
            (map_[y + 1][x - 1] == Pierre) and
            (map_[y + 2][x - 2] != Pierre and map_[y + 2][x - 2] != '+') and
            (map_[y - 1][x + 1] == '+')):
            return True
        if ((y >= 1 and y <= 16 and x >= 1 and x <= 14) and
            (map_[y + 1][x + 1] == Pierre) and
            (map_[y + 2][x + 2] != Pierre and map_[y + 2][x + 2] != '+') and
            (map_[y - 1][x - 1] == '+')):
            return True
        if ((y >= 2 and y <= 17 and x >= 4 and x <= 17) and
            (map_[y - 1][x + 1] == Pierre) and
            (map_[y - 2][x + 2] != Pierre and map_[y - 2][x + 2] != '+') and
            (map_[y + 1][x - 1] == '+')):
            return True
        return False

    def checkCaptureDiagH2(self, map_, y, x):
        Pierre = map_[y][x]
        if ((y >= 1 and y <= 16 and x >= 2 and x <= 14) and
            (map_[y + 1][x - 1] == Pierre) and
            (map_[y + 2][x - 2] != Pierre and map_[y + 2][x - 2] != '+') and
            (map_[y - 1][x + 1] == '+')):
            return True
        if ((y >= 1 and y <= 16 and x >= 1 and x <= 14) and
            (map_[y + 1][x + 1] == Pierre) and
            (map_[y + 2][x + 2] != Pierre and map_[y + 2][x + 2] != '+') and
            (map_[y - 1][x - 1] == '+')):
            return True
        if ((y >= 2 and y <= 17 and x >= 1 and x <= 14) and
            (map_[y - 1][x + 1] == Pierre) and
            (map_[y - 2][x + 2] != Pierre and map_[y - 2][x + 2] != '+') and
            (map_[y + 1][x - 1] == '+')):
            return True
        if ((y >= 2 and y <= 17 and x >= 2 and x <= 14) and
            (map_[y - 1][x - 1] == Pierre) and
            (map_[y - 2][x - 2] != Pierre and map_[y - 2][x - 2] != '+') and
            (map_[y + 1][x + 1] == '+')):
            return True
        return False

    def checkCaptureHori(self, map_, y, x, flag):
        Pierre = map_[y][x]
        if ((x >= 2 and x <= 17) and
            (map_[y][x - 1] == Pierre) and
            (map_[y][x - 2] != Pierre and map_[y][x - 2] != '+') and
            (map_[y][x + 1] == '+')):
            return True
        if ((x >= 1 and x <= 16) and
            (map_[y][x + 1] == Pierre) and
            (map_[y][x + 2] != Pierre and map_[y][x + 2] != '+') and
            (map_[y][x - 1] == '+')):
            return True
        if ((y >= 4 and y <= 18 and x >= 1 and x <= 16 and flag == 0) and
            (map_[y - 1][x + 1] == Pierre) and
            (map_[y - 1][x + 2] != Pierre and map_[y - 1][x + 2] != '+') and
            (map_[y - 1][x - 1] == '+')):
            return True
        if ((y >= 4 and y <= 18 and x >= 2 and x <= 17 and flag == 0) and
            (map_[y - 1][x - 1] == Pierre) and
            (map_[y - 1][x - 2] != Pierre and map_[y - 1][x - 2] != '+') and
            (map_[y - 1][x + 1] == '+')):
            return True
        if ((y >= 0 and y <= 14 and x >= 1 and x <= 16 and flag == 0) and
            (map_[y + 1][x + 1] == Pierre) and
            (map_[y + 1][x + 2] != Pierre and map_[y + 1][x + 2] != '+') and
            (map_[y + 1][x - 1] == '+')):
            return True
        if ((y >= 0 and y <= 14 and x >= 2 and x <= 17 and flag == 0) and
            (map_[y + 1][x - 1] == Pierre) and
            (map_[y + 1][x - 2] != Pierre and map_[y + 1][x - 2] != '+') and
            (map_[y + 1][x + 1] == '+')):
            return True
        return False

    def checkCaptureDiagV1(self, map_, y, x):
        Pierre = map_[y][x]
        if ((y >= 2 and y <= 14 and x >= 2 and x <= 17) and
            (map_[y - 1][x - 1] == Pierre) and
            (map_[y - 2][x - 2] != Pierre and map_[y - 2][x - 2] != '+') and
            (map_[y + 1][x + 1] == '+')):
            return True
        if ((y >= 1 and y <= 14 and x >= 2 and x <= 17) and
            (map_[y + 1][x - 1] == Pierre) and
            (map_[y + 2][x - 2] != Pierre and map_[y + 2][x - 2] != '+') and
            (map_[y - 1][x + 1] == '+')):
            return True
        if ((y >= 1 and y <= 14 and x >= 1 and x <= 16) and
            (map_[y + 1][x + 1] == Pierre) and
            (map_[y + 2][x + 2] != Pierre and map_[y + 2][x + 2] != '+') and
            (map_[y - 1][x - 1] == '+')):
            return True
        if ((y >= 2 and y <= 14 and x >= 1 and x <= 16) and
            (map_[y - 1][x + 1] == Pierre) and
            (map_[y - 2][x + 2] != Pierre and map_[y - 2][x + 2] != '+') and
            (map_[y + 1][x - 1] == '+')):
            return True
        return False

    def checkCaptureDiagV2(self, map_, y, x):
        Pierre = map_[y][x]
        if ((y >= 4 and y <= 17 and x >= 2 and x <= 17) and
            (map_[y - 1][x - 1] == Pierre) and
            (map_[y - 2][x - 2] != Pierre and map_[y - 2][x - 2] != '+') and
            (map_[y + 1][x + 1] == '+')):
            return True
        if ((y >= 4 and y <= 16 and x >= 2 and x <= 17) and
            (map_[y + 1][x - 1] == Pierre) and
            (map_[y + 2][x - 2] != Pierre and map_[y + 2][x - 2] != '+') and
            (map_[y - 1][x + 1] == '+')):
            return True
        if ((y >= 4 and y <= 16 and x >= 1 and x <= 16) and
            (map_[y + 1][x + 1] == Pierre) and
            (map_[y + 2][x + 2] != Pierre and map_[y + 2][x + 2] != '+') and
            (map_[y - 1][x - 1] == '+')):
            return True
        if ((y >= 4 and y <= 17 and x >= 1 and x <= 16) and
            (map_[y - 1][x + 1] == Pierre) and
            (map_[y - 2][x + 2] != Pierre and map_[y - 2][x + 2] != '+') and
            (map_[y + 1][x - 1] == '+')):
            return True
        return False

    def CaptureCheckH(self, map_, y, x): #check si le 5 est cassable / Returne True si oui, false si non
        if ((self.checkCaptureVert(map_, y, x, 0) == True) or
            (self.checkCaptureDiagH1(map_, y, x) == True) or
            (self.checkCaptureDiagH2(map_, y, x) == True) or
            (self.checkCaptureDiagV1(map_, y, x) == True) or
            (self.checkCaptureDiagV2(map_, y, x) == True)):
            return True
        else:
            return False

    def CaptureCheckD(self, map_, y, x): #check si le 5 est cassable / Returne True si oui, false si non
        if ((self.checkCaptureVert(map_, y, x, 1) == True) or
            (self.checkCaptureDiagH1(map_, y, x) == True) or
            (self.checkCaptureDiagH2(map_, y, x) == True) or
            (self.checkCaptureDiagV1(map_, y, x) == True) or
            (self.checkCaptureDiagV2(map_, y, x) == True) or
            (self.checkCaptureHori(map_, y, x, 1) == True)):
            return True
        else:
            return False

    def CaptureCheckV(self, map_, y, x): #check si le 5 est cassable / Returne True si oui, false si non
        if ((self.checkCaptureHori(map_, y, x, 1) == True) or
            (self.checkCaptureDiagH1(map_, y, x) == True) or
            (self.checkCaptureDiagH2(map_, y, x) == True) or
            (self.checkCaptureDiagV1(map_, y, x) == True) or
            (self.checkCaptureDiagV2(map_, y, x) == True)):
            return True
        else:
            return False

    def gameEnd(self, map_, lastY, lastX):
        #checkCapture => With return True
        for y, l in enumerate(map_):
            for x, sym in enumerate(l):
                if ((map_[y][x] != '+') and ((self.checkHori(map_, y, x)))): #CheckHori and diad
                    if (self.CaptureCheckH(map_, lastY, lastX) == False):
                        return map_[y][x]
                elif ((map_[y][x] != '+') and (self.checkVert(map_, y, x))): #Check Vert
                    if (self.CaptureCheckV(map_, lastY, lastX) == False):
                        return map_[y][x]
                elif ((map_[y][x] != '+') and ((self.checkVert(map_, y, x)) or #CheckDiag
                                            (self.checkDiag(map_, y, x)) or
                                            (self.checkDiag2(map_, y, x)))):
                    if (self.CaptureCheckD(map_, lastY, lastX) == False):
                        return map_[y][x]
        return '+'

    def countDiag1(self, X, Y, newMap, symb): # de Haut/Gauche à Bas/Droite
        nb = 0 # Remonter jusqu'à (possible) bord
        for i in range(0, 3):
            if X != 0 and Y != 0:
                X -= 1
                Y -= 1
        Xor = X
        Yor = Y
        for i in range (0, 7):
            if newMap[Y][X] != symb and newMap[Y][X] != '+':
                nb = 0
            if X < 18 and Y < 18:
                X += 1
                Y += 1
                nb += 1
        if nb >= 5:
            nb = 0
            X = Xor
            Y = Yor
            for i in range(0, 7):
                if X == 18 or Y == 18:
                    break
                if newMap[Y][X] == symb:
                    nb = nb + 1
                elif newMap[Y][X] != '+':
                    nb = 0
                X += 1
                Y += 1
            if nb == 3:
                return 1
        return 0

    def countDiag2(self, X, Y, newMap, symb): # de Bas/Gauche à Haut/Droite
        nb = 0
        for i in range(0, 3):
            if X != 0 and Y != 18:
                X -= 1
                Y += 1
        Xor = X
        Yor = Y
        for i in range (0, 7):
            if newMap[Y][X] != symb and newMap[Y][X] != '+':
                nb = 0
            if X < 18 and Y > 0:
                X += 1
                Y -= 1
                nb += 1
        if nb >= 5:
            nb = 0
            X = Xor
            Y = Yor
            for i in range(0, 7):
                if X > 18 or Y < 0:
                    break
                if newMap[Y][X] == symb:
                    nb = nb + 1
                elif newMap[Y][X] != '+':
                    nb = 0
                X = X + 1
                Y = Y - 1
            if nb == 3:
                return 1
        return 0

    def countHori(self, X, Y, newMap, symb):
        nb = 0
        for i in range(0, 3):
            if X != 0:
                X -= 1
        Xor = X
        for i in range (0, 7):
            if newMap[Y][X] != symb and newMap[Y][X] != '+':
                nb = 0
            if X < 18:
                X += 1
                nb += 1
        if nb >= 5:
            nb = 0
            X = Xor
            for i in range(0, 7):
                if X == 18:
                    break
                if newMap[Y][X] == symb:
                    nb = nb + 1
                elif newMap[Y][X] != '+':
                    nb = 0
                X = X + 1
            if nb == 3:
                return 1
        return 0

    def countVert(self, X, Y, newMap, symb):
        nb = 0
        for i in range(0, 3):
            if Y != 0:
                Y -= 1
        Yor = Y
        for i in range (0, 7):
            if newMap[Y][X] != symb and newMap[Y][X] != '+':
                nb = 0
            if Y < 18:
                Y += 1
                nb += 1
        if nb >= 5:
            nb = 0
            Y = Yor
            for i in range(0, 7):
                if Y == 18:
                    break
                if newMap[Y][X] == symb:
                    nb = nb + 1
                elif newMap[Y][X] != '+':
                    nb = 0
                Y = Y + 1
            if nb == 3:
                return 1
        return 0

    def threeRule(self, X, Y, newMap, symb):
        nb = 0
        verif = True
        if (X - 3 < 0 and Y - 3 < 0) and (X + 3 > 19 and Y + 3 > 19):
            verif = False
        if verif == True:
            nb += self.countDiag1(X, Y, newMap, symb) #Verif 1 / 4

        verif = True
        if (X - 3 < 0 and Y + 3 < 0) and (X + 3 > 19 and Y - 3 > 19):
            verif = False
        if verif == True:
            nb += self.countDiag2(X, Y, newMap, symb) #Verif 2 / 4

        nb += self.countHori(X, Y, newMap, symb) #Verif 3 / 4

        nb += self.countVert(X, Y, newMap, symb) #Verif 4 / 4

        if nb >= 2:
            return 2
        return 0

    def captHori(self, y, x, newMap): #Fonction annexe capture horizontal
        ret = 0
        Pierre = newMap[y][x]
        if ((x >= 3 and newMap[y][x - 1] != '+' and newMap[y][x - 1] != Pierre) and
            (newMap[y][x - 2] != '+' and newMap[y][x - 2] != Pierre) and
            (newMap[y][x - 3] == Pierre)):
            ret += 1
            newMap[y][x - 1] = '+'
            newMap[y][x - 2] = '+'
        if ((x <= 15 and newMap[y][x + 1] != '+' and newMap[y][x + 1] != Pierre) and
            (newMap[y][x + 2] != '+' and newMap[y][x + 2] != Pierre) and
            (newMap[y][x + 3] == Pierre)):
            ret += 1
            newMap[y][x + 1] = '+'
            newMap[y][x + 2] = '+'
        return ret

    def captVert(self, y, x, newMap):
        ret = 0
        Pierre = newMap[y][x]
        if ((y >= 3 and newMap[y - 1][x] != '+' and newMap[y - 1][x] != Pierre) and
            (newMap[y - 2][x] != '+' and newMap[y - 2][x] != Pierre) and
            (newMap[y - 3][x] == Pierre)):
            ret += 1
            newMap[y - 1][x] = '+'
            newMap[y - 2][x] = '+'
        if ((y <= 15 and newMap[y + 1][x] != '+' and newMap[y + 1][x] != Pierre) and
            (newMap[y + 2][x] != '+' and newMap[y + 2][x] != Pierre) and
            (newMap[y + 3][x] == Pierre)):
            ret += 1
            newMap[y + 1][x] = '+'
            newMap[y + 2][x] = '+'
        return ret

    def captDiag1(self, y, x, newMap):
        ret = 0
        Pierre = newMap[y][x]
        if ((y >= 3 and x >= 3) and
            (newMap[y - 1][x - 1] != '+' and
             newMap[y - 1][x - 1] != Pierre) and
            (newMap[y - 2][x - 2] != '+' and
             newMap[y - 2][x - 2] != Pierre) and
            (newMap[y - 3][x - 3] == Pierre)):
            ret += 1
            newMap[y - 1][x - 1] = '+'
            newMap[y - 2][x - 2] = '+'
        if ((y <= 15 and x <= 15) and
            (newMap[y + 1][x + 1] != '+' and
             newMap[y + 1][x + 1] != Pierre) and
            (newMap[y + 2][x + 2] != '+' and newMap[y + 2][x + 2] != Pierre) and
            (newMap[y + 3][x + 3] == Pierre)):
            ret += 1
            newMap[y + 1][x + 1] = '+'
            newMap[y + 2][x + 2] = '+'
        return ret

    def captDiag2(self, y, x, newMap):
        ret = 0
        Pierre = newMap[y][x]
        if ((y >= 3 and x <= 15 and newMap[y - 1][x + 1] != '+' and newMap[y - 1][x + 1] != Pierre) and
            (newMap[y - 2][x + 2] != '+' and newMap[y - 2][x + 2] != Pierre) and
            (newMap[y - 3][x + 3] == Pierre)):
            ret += 1
            newMap[y - 1][x + 1] = '+'
            newMap[y - 2][x + 2] = '+'
        if ((y <= 15 and x >= 3 and newMap[y + 1][x - 1] != '+' and newMap[y + 1][x - 1] != Pierre) and
            (newMap[y + 2][x - 2] != '+' and newMap[y + 2][x - 2] != Pierre) and
            (newMap[y + 3][x - 3] == Pierre)):
            ret += 1
            newMap[y + 1][x - 1] = '+'
            newMap[y + 2][x - 2] = '+'
        return ret

    def CaptureFunct(self, y, x, newMap):
        ret = 0
        ret += self.captHori(y, x, newMap)
        ret += self.captVert(y, x, newMap)
        ret += self.captDiag1(y, x, newMap)
        ret += self.captDiag2(y, x, newMap)
        return ret

    def limitPose(self, oldPlay, play, newMap):
        y, x = oldPlay
        tab = ['self.isLeftUpFree(newMap, y, x)', 'self.isUpFree(newMap, y, x)', 'self.isRightUpFree(newMap, y, x)',\
               'self.isLeftFree(newMap, y, x)', 'self.isRightFree(newMap, y, x)',\
               'self.isLeftDownFree(newMap, y, x)', 'self.isDownFree(newMap, y, x)', 'self.isRightDownFree(newMap, y, x)']
        postab = []
        for rand in range(0,8):
            res = eval(tab[rand])
            postab.append(res) if res else None
        return play in postab

    def validPlayerMove(self, oldPlay, play, old, newMap):
        if not oldPlay and play != (9, 9):
            return 3
        elif not oldPlay:
            return 0
        if not self.limitPose(oldPlay, play, old):
            return 1
        Y = play[0]
        X = play[1]
        if self.threeRule(X, Y, newMap, newMap[Y][X]) == 2:
            return 2
        self.CaptureFunct(Y, X, newMap)
        return 0
