﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGun : MonoBehaviour {

	public GameObject EnemyBullet;

	// Use this for initialization
	void Start () {
		Invoke("FireEnemyBullet", 0.5f);
		Invoke("FireEnemyBullet", 2f);
		Invoke("FireEnemyBullet", 5f);
		Invoke("FireEnemyBullet", 7f);
	}
	
	// Update is called once per frame
	void Update () {
	}

	void FireEnemyBullet() {
		GameObject PlayerShip = GameObject.Find("player");

		if (PlayerShip != null) {
			GameObject bullet = (GameObject)Instantiate(EnemyBullet);
			bullet.transform.position = transform.position;

			Vector2 direction = PlayerShip.transform.position - bullet.transform.position;
			bullet.GetComponent<EnemyBullet>().SetDirection(direction);
		}
	}
}