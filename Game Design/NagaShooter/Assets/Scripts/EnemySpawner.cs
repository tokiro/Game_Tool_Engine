﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {

	public GameObject Enemy;

	float maxSpawnRateInSeconds = 10f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void SpawnEnemy() {
		Vector2 min = Camera.main.ViewportToWorldPoint (new Vector2 (0.30f, 0));//bot left corner
		Vector2 max = Camera.main.ViewportToWorldPoint (new Vector2 (0.75f, 1));//top right corner

		GameObject anEnemy = (GameObject)Instantiate (Enemy);
		anEnemy.transform.position = new Vector2(Random.Range(min.x, max.x), max.y);

		ScheduleNextEnemySpawn();
	}

	void ScheduleNextEnemySpawn() {
		float spawnInNSeconds = 0.8f;

		if (spawnInNSeconds > 1.1f) {
			spawnInNSeconds = Random.Range(1f, spawnInNSeconds);
		}
		else {
			spawnInNSeconds = 0.8f;
		}

		Invoke("SpawnEnemy", spawnInNSeconds);
	}

	void IncreaseSpawnRate() {
		if (maxSpawnRateInSeconds > 1f) {
			maxSpawnRateInSeconds --;
		}
		if (maxSpawnRateInSeconds == 1f) {
			CancelInvoke("IncreaseSpawnRate");
		}
	}

    public void ScheduleEnemySpawner()
    {
        maxSpawnRateInSeconds = 0f;
        Invoke("SpawnEnemy", maxSpawnRateInSeconds);

        InvokeRepeating("IncreaseSpawnRate", 0f, 30f);
    }

    public void UnscheduleEnemySpawner()
    {
        CancelInvoke("SpawnEnemy");
        CancelInvoke("IncreaseSpawnRate");
    }
}
