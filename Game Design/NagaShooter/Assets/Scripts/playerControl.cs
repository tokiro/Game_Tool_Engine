﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

#pragma strict 

public class playerControl : MonoBehaviour {

    public GameObject MYGameManager;

	public GameObject PlayerBullet;
	public GameObject bulletPosition01;
//	public GameObject bulletPosition02;
    public GameObject playerExplosion;

    public Text LivesUIText;

    const int MaxLives = 5;
    int lives;

	int shootTime;

	public float speed;


    public void Init()
    {
        lives = MaxLives;
        LivesUIText.text = lives.ToString();

        transform.position = new Vector2(0, -4);

        gameObject.SetActive(true);
    }

    // Use this for initialization
    void Start () {
		shootTime = 0;
	}
	
	// Update is called once per frame
	void Update () {

		float x = Input.GetAxis("Horizontal");
		float y = Input.GetAxis("Vertical");

		Vector2 direction = new Vector2(x, y);
		if (direction.sqrMagnitude > 1)
			direction.Normalize();

		if (shootTime == 25)
		{
			Shoot();
			shootTime = 0;
		}
		else
			shootTime++;

		Move(direction);
	}

	void Move(Vector2 direction) {

		Vector2 min = Camera.main.ViewportToWorldPoint (new Vector2 (0.25f, 0));//bot left corner
		Vector2 max = Camera.main.ViewportToWorldPoint (new Vector2 (0.80f, 0.20f));//top right corner

		max.x = max.x - 0.5f;
		min.x = min.x + 0.5f;

		max.y = max.y - 0.5f;
		min.y = min.y + 0.5f;

		Vector2 pos = transform.position;

		pos += direction * speed * Time.deltaTime;

		pos.x = Mathf.Clamp (pos.x, min.x, max.x);
		pos.y = Mathf.Clamp (pos.y, min.y, max.y);

		transform.position = pos;
	}
		

    public void Shoot()
    {
        GetComponent<AudioSource>().Play();
        GameObject bullet01 = (GameObject)Instantiate(PlayerBullet);
        bullet01.transform.position = bulletPosition01.transform.position;

        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if ((collision.tag == "EnemyShipTag") || (collision.tag == "EnemyBulletTag"))
        {
            PlayExplosion();
            lives--;
            LivesUIText.text = lives.ToString();

            if (lives == 0)
            {
                MYGameManager.GetComponent<GameManager>().SetGameManagerState(GameManager.GameManagerState.GameOver);

                gameObject.SetActive(false);
            }
        }
    }

    void PlayExplosion()
    {
        GameObject explosion = (GameObject)Instantiate (playerExplosion);

        explosion.transform.position = transform.position;
    }
}
