﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyControl : MonoBehaviour {

    public GameObject enemyExplosion;
    GameObject ScoreUIText;

    float speed;
	float i = 0;
	float Move = 0.01f;

	// Use this for initialization
	void Start () {
		speed = 1f;

        ScoreUIText = GameObject.FindGameObjectWithTag("ScoreTextTag");
	}
	
	// Update is called once per frame
	void Update () {

		if (Random.Range(1, 100) == 1)
			Move = -Move;
		Vector2 position = transform.position;

		position = new Vector2(position.x + Move, position.y - speed * Time.deltaTime);
		//position = new Vector2(position.x, position.y - speed * Time.deltaTime);

		transform.position = position;

		Vector2 min = Camera.main.ViewportToWorldPoint (new Vector2 (0, 0));//bot left corner

		if (transform.position.y < min.y) {
			Destroy(gameObject);
		}
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if ((collision.tag == "PlayerShipTag") || (collision.tag == "PlayerBulletTag"))
        {
            PlayExplosion();

            ScoreUIText.GetComponent<GameScore>().Score += 100;

            Destroy(gameObject);
        }
    }

    void PlayExplosion()
    {
        GameObject explosion = (GameObject)Instantiate(enemyExplosion);

        explosion.transform.position = transform.position;
    }
}