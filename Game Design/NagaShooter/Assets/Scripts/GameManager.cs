﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

	public GameObject playButton;
	public GameObject playerShip;
	public GameObject enemySpawner;
	public GameObject GameOver;
	public GameObject ScoreUIText;
	public GameObject MYTimeCounter;
	//public GameObject ShootButton;

	public enum GameManagerState
	{
		Opening,
		GamePlay,
		GameOver,
	}

	GameManagerState GMState;

	// Use this for initialization
	void Start () {
		GMState = GameManagerState.Opening;
	}

	// Update is called once per frame
	void Update () {

	}

	void UpdateGameManagerState()
	{
		switch(GMState)
		{
		case GameManagerState.Opening:

			playButton.SetActive(true);
			GameOver.SetActive(false);

			break;
		case GameManagerState.GamePlay:

			ScoreUIText.GetComponent<GameScore>().Score = 0;
			playButton.SetActive(false);
			//ShootButton.SetActive(true);
			playerShip.GetComponent<playerControl>().Init();
			enemySpawner.GetComponent<EnemySpawner>().ScheduleEnemySpawner();

			//MYTimeCounter.GetComponent<TimeCounter>().StartTimeCounter();

			break;
		case GameManagerState.GameOver:

			//MYTimeCounter.GetComponent<TimeCounter> ().StopTimeCounter ();
			enemySpawner.GetComponent<EnemySpawner> ().UnscheduleEnemySpawner ();
			GameObject[] gameObjects = GameObject.FindGameObjectsWithTag ("EnemyShipTag");
			foreach(GameObject enemy in gameObjects) 
				Destroy (enemy);
			GameOver.SetActive(true);
			//ShootButton.SetActive(false);
			Invoke("ChangeToOpenningState", 2f);

			break;
		}
	}

	public void SetGameManagerState(GameManagerState state)
	{
		GMState = state;
		UpdateGameManagerState();
	}

	public void StartGamePlay()
	{
		GMState = GameManagerState.GamePlay;
		UpdateGameManagerState();
	}

	public void ChangeToOpenningState()
	{
		SetGameManagerState(GameManagerState.Opening);
	}
}